FlyPy Aerodynamics Package - Thin Airfoil Solver

 - Provide an airfoil camberline as camberline.txt (see airfoils package)
 - Specify the desired angles of attack in thin_airfoil.py
 - Run the code using "python thin_airfoil.py"
 - Plots and data are saved in their respective sub folders