FlyPy Aerodynamics Package - Vortex Panel Solver

 - Provide an airfoil geometry as airfoil.txt (see airfoils package)
 - Specify the desired angles of attack in thin_airfoil.py
 - Run the code using "python vortex_panel.py"
 - Plots and data are saved in their respective sub folders