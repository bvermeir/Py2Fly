{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4f3113bf",
   "metadata": {},
   "source": [
    "# Thin Airfoil Theory\n",
    "This notebook will use thin airfoil theory to predict the circulation distribution, lift coefficient, moment coefficient at the quarter chord, and center of pressure.\n",
    "\n",
    "The user must provide:\n",
    " - The airfoil camberline coordinates normalized by the chord length in camberline.txt (see airfoil generators notebook)\n",
    " - The desired minimum, maximum and increments for the angle of attack (minalpha, maxalpha, alphastep)\n",
    " \n",
    "The outputs of this program include\n",
    " - Plots of the circulation distribution, lift coefficient, moment coefficient at the quarter chord, and center of pressure saved in the plots folder.\n",
    " - Table of the above values saved in the data folder.\n",
    " \n",
    "Once those are provided you simply need to run all the cells in the notebook to generate the output. A NACA 0012 demonstration case is included by default."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce102505",
   "metadata": {},
   "source": [
    "Importing a few useful libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd123d08",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04880cdf",
   "metadata": {},
   "source": [
    "Defining a function to do numerical integration using the trapezoidal rule"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb047f76",
   "metadata": {},
   "outputs": [],
   "source": [
    "def integrate(x, y):\n",
    "    # Performs a second-order trapezoidal rule integration\n",
    "    val = 0\n",
    "    for i in range(0, np.shape(x)[0] - 1):\n",
    "        val = val + 0.5 * (y[i] + y[i + 1]) * (x[i + 1] - x[i])\n",
    "\n",
    "    return val"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3faf704c",
   "metadata": {},
   "source": [
    "The core thin airfoil routine"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1e3ac1d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "def thin_airfoil(alpha, plotgam, x_c, theta, dz_dx):\n",
    "    # Convert alpha to radians\n",
    "    alpha = alpha / 180 * math.pi\n",
    "\n",
    "    # Generate the spectral coefficient array\n",
    "    A = np.zeros(math.floor(np.shape(x_c)[0] / 2))\n",
    "\n",
    "    # Compute the integrals for each A coefficient\n",
    "    A[0] = alpha - 1.0 / math.pi * integrate(theta, dz_dx)\n",
    "    for i in range(1, np.shape(A)[0]):\n",
    "        A[i] = 2.0 / math.pi * integrate(theta, np.multiply(dz_dx, np.cos(i * theta)))\n",
    "\n",
    "    # Generate the gamma over U distribution to plot skipping the leading edge\n",
    "    gam = np.zeros(np.shape(x_c)[0])\n",
    "    for i in range(1, np.shape(x_c)[0]):\n",
    "        gam[i] = A[0] * ((1 + math.cos(theta[i])) / (math.sin(theta[i])))\n",
    "        for j in range(1, np.shape(A)[0]):\n",
    "            gam[i] = gam[i] + A[j] * math.sin(j * theta[i])\n",
    "    gam = 2 * gam\n",
    "\n",
    "    cl = math.pi * (2 * A[0] + A[1])\n",
    "    cm_c4 = math.pi / 4 * (A[2] - A[1])\n",
    "    x_cp = 1 / 4 * (1 + math.pi / cl * (A[1] - A[2]))\n",
    "\n",
    "    # Plot the gamma distribution\n",
    "    plotgam.plot(x_c[1:-1, 0], gam[1:-1], label=r'$\\alpha = $' + str(alpha / math.pi * 180))\n",
    "    plotgam.set_xlabel('x/c')\n",
    "    plotgam.set_ylabel(r'$\\gamma / U_\\infty$')\n",
    "    plotgam.set_title(r'$\\gamma / U_\\infty$ Distribution')\n",
    "    plotgam.set_ylim([-1, 2])\n",
    "    plotgam.legend(loc=\"upper right\", ncol=3)\n",
    "    np.savetxt('data/gamma_' + str(alpha / math.pi * 180) + '.txt', np.c_[x_c[1:-1, 0], gam[1:-1]], delimiter=' ')\n",
    "\n",
    "    return cl, cm_c4, x_cp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc786cc0",
   "metadata": {},
   "source": [
    "Define a function to call the thin-airfoil routine for a range of angles of attack"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3af29003",
   "metadata": {},
   "outputs": [],
   "source": [
    "def sweep(minalpha, maxalpha, alphastep, plotgam):\n",
    "    # Initialize plotting arrays\n",
    "    alpha_plot = []\n",
    "    cl_plot = []\n",
    "    cm_c4_plot = []\n",
    "    x_cp_plot = []\n",
    "\n",
    "    # Start by loading the camber line points from disk\n",
    "    x_c = np.loadtxt('camberline.txt')\n",
    "\n",
    "    # Compute the corresponding theta positions\n",
    "    theta = np.arccos(1 - 2 * x_c[:, 0])\n",
    "\n",
    "    # Approximate the slope of the camber line\n",
    "    dz_dx = np.zeros(np.shape(x_c)[0])\n",
    "    dz_dx[0] = (x_c[1, 1] - x_c[0, 1]) / (x_c[1, 0] - x_c[0, 0])\n",
    "    dz_dx[-1] = (x_c[-1, 1] - x_c[-2, 1]) / (x_c[-1, 0] - x_c[-2, 0])\n",
    "    for i in range(1, np.shape(x_c)[0] - 1):\n",
    "        dz_dx[i] = (x_c[i + 1, 1] - x_c[i, 1]) / (x_c[i + 1, 0] - x_c[i, 0])\n",
    "\n",
    "    # Sweep over a range of angles of attack\n",
    "    alpha = minalpha\n",
    "    while alpha <= maxalpha:\n",
    "        [cl, cm_c4, x_cp] = thin_airfoil(alpha, plotgam, x_c, theta, dz_dx)\n",
    "\n",
    "        # Append data to the plotting arrays\n",
    "        alpha_plot.append(alpha)\n",
    "        cl_plot.append(cl)\n",
    "        cm_c4_plot.append(cm_c4)\n",
    "        x_cp_plot.append(x_cp)\n",
    "\n",
    "        alpha = alpha + alphastep\n",
    "\n",
    "    # Plot the lift, moment, and center of pressure and save data\n",
    "    figure, plot = plt.subplots()\n",
    "    plot.plot(alpha_plot, cl_plot, marker=\".\", markersize=6, label=r'$C_L$')\n",
    "    plot.legend()\n",
    "    plot.set_xlabel(r'$\\alpha$')\n",
    "    plot.set_ylabel(r'$C_L$')\n",
    "    plot.set_title('Lift Curve')\n",
    "    figure.savefig('plots/cl.pdf')\n",
    "    np.savetxt('data/cl.txt', np.c_[alpha_plot, cl_plot], delimiter=' ')\n",
    "\n",
    "    figure, plot = plt.subplots()\n",
    "    plot.plot(alpha_plot, cm_c4_plot, marker=\".\", markersize=6, label=r'$C_{m,c/4}$')\n",
    "    plot.legend()\n",
    "    plot.set_xlabel(r'$\\alpha$')\n",
    "    plot.set_ylabel(r'$C_{m,c/4}$')\n",
    "    plot.set_title('Quarter Chord Moment Coefficient Curve')\n",
    "    figure.savefig('plots/cm_c4.pdf')\n",
    "    np.savetxt('data/cm_c4.txt', np.c_[alpha_plot, cm_c4_plot], delimiter=' ')\n",
    "\n",
    "    figure, plot = plt.subplots()\n",
    "    plot.plot(alpha_plot, x_cp_plot, marker=\".\", markersize=6, label=r'$x_{cp}/c$')\n",
    "    plot.legend()\n",
    "    plot.set_xlabel(r'$\\alpha$')\n",
    "    plot.set_ylabel(r'$x_{cp}/c$')\n",
    "    plot.set_title('Center of Pressure Curve')\n",
    "    figure.savefig('plots/x_cp.pdf')\n",
    "    np.savetxt('data/x_cp.txt', np.c_[alpha_plot, x_cp_plot], delimiter=' ')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "237c2817",
   "metadata": {},
   "source": [
    "Initialize some subplots"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2140dee4",
   "metadata": {},
   "source": [
    "Sweep over a range of desired angles of attack and plot the performance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fc7b79a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize the gamma plot\n",
    "figgam, plotgam = plt.subplots()\n",
    "\n",
    "# Sweep over the desired angles of attack\n",
    "minalpha = -6\n",
    "maxalpha = 10\n",
    "alphastep = 1\n",
    "sweep(minalpha, maxalpha, alphastep, plotgam)\n",
    "\n",
    "# Show all the plots\n",
    "plt.show()\n",
    "figgam.savefig('plots/gamma.pdf')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4ed5757e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
