{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e0bef6c3",
   "metadata": {},
   "source": [
    "# Static Stability\n",
    "This notebook will use basic aircraft parameters to predict static stability parameters\n",
    "\n",
    "The user must provide:\n",
    " - A range of parameters defining the geometry and performance of the aircraft\n",
    " \n",
    "The outputs of this program include\n",
    " - The static margin for pitch stability\n",
    " - The yaw derivative for yaw stability\n",
    " - The roll derivative for roll stability\n",
    " \n",
    "Once those are provided you simply need to run all the cells in the notebook to generate the output, which are printed to the notebook. It is important to note that dynamic stability should also be considered for aircraft design."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecf98b68",
   "metadata": {},
   "source": [
    "Load some useful libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a269045a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d27e16af",
   "metadata": {},
   "source": [
    "Provide parameters about the specific aircraft"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4cd8b506",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Aircraft parameters\n",
    "cbar_w = 1.66255  # Mean chord of the main wing (m)\n",
    "s_w = 16.7225  # Wing area (m^2)\n",
    "s_h = 3.34451  # Horizontal stabilizer area (m^2)\n",
    "s_v = 1.105546  # Vertical stabilizer area (m^2)\n",
    "b_w = 10.0584  # Span of the main wing (m)\n",
    "b_h = 3.6576  # Span of the horizontal stabilizer (m)\n",
    "h_v = 0.9144  # Distance above center of gravity to the aerodynamic center of the tail\n",
    "c_lw_alpha = 4.44  # Lift slope of the main wing\n",
    "c_lh_alpha = 3.97  # Lift slope of the horizontal stabilizer\n",
    "c_lv_alpha = 3.40  # Lift slope of the horizontal stabilizer\n",
    "l_w = -0.216408  # Distance aft of the center of gravity to aerodynamic center of the main wing (m)\n",
    "l_h = 4.355592  # Distance aft of the center of gravity to aerodynamic center of the horizontal stabilizer (m)\n",
    "l_v = 4.514088  # Distance aft of the center of gravity to aerodynamic center of the vertical stabilizer (m)\n",
    "eta_h = 1.0  # Dynamic pressure ratio relative to the free stream on the horizontal stabilizer\n",
    "eta_v = 1.0  # Dynamic pressure ratio relative to the free stream on the vertical stabilizer\n",
    "eps_d_alpha = 0.44  # Down wash gradient, or the change in down wash with angle of attack\n",
    "eps_s_beta_v = -0.10  # Side wash gradient, or the change in side wash with angle of attack\n",
    "gamma = -0.1  # Wing dihedral angle (degrees)\n",
    "kappa_l = 1.07  # Wing dihedral factor (See Figure 5.6.3 of Phillips)\n",
    "kappa_gamma = 0.83  # Wing dihedral factor (See Figure 5.6.3 of Phillips)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84bdf5b0",
   "metadata": {},
   "source": [
    "Define the static margin function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "797f96e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def static_margin(s_w, s_h, c_lw_alpha, c_lh_alpha, l_w, l_h, eta_h, eps_d_alpha, cbar_w):\n",
    "    # Example 4.4.1 of Phillips\n",
    "    sm = (l_w * c_lw_alpha + (s_h * l_h) / (s_w) * eta_h * c_lh_alpha * (1 - eps_d_alpha)) / (\n",
    "            cbar_w * (c_lw_alpha + s_h / s_w * eta_h * c_lh_alpha * (1 - eps_d_alpha)))\n",
    "\n",
    "    # Print the static margin\n",
    "    print('Static Margin: ', round(sm * 100, 2), '% (recommended 5->15%)', sep='')\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a387abf2",
   "metadata": {},
   "source": [
    "Define the yaw derivative function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa866a37",
   "metadata": {},
   "outputs": [],
   "source": [
    "def yaw_derivative(eta_v, s_v, s_w, l_v, l_w, b_w, c_lv_alpha, eps_s_beta_v):\n",
    "    # Equation 5.2.7 of Phillips\n",
    "    deltac_n_beta_v = eta_v * (s_v * l_v) / (s_w * b_w) * c_lv_alpha * (1 - eps_s_beta_v)\n",
    "\n",
    "    # Print the yaw stability derivative\n",
    "    print('Yaw Stability Derivative: ', round(deltac_n_beta_v, 3),\n",
    "          ' (recommended 0.06->0.15, vertical stabilizer contribution only!)', sep='')\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffd546cc",
   "metadata": {},
   "source": [
    "Define the roll derivative function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04c7c639",
   "metadata": {},
   "outputs": [],
   "source": [
    "def roll_derivative(gamma, kappa_gamma, kappa_l, c_lw_alpha, h_v):\n",
    "    # Convert the dihedral angle to radians\n",
    "    gamma = gamma * math.pi / 180\n",
    "\n",
    "    # Get the contribution from the main wing, assuming negligible sweep, via Equation 5.6.13 in Phillips\n",
    "    deltac_l_beta_gammaw = -(2 * math.sin(gamma)) / (\n",
    "            3 * math.pi * math.cos(gamma) ** 4) * kappa_gamma * kappa_l * c_lw_alpha\n",
    "\n",
    "    # Get the contribution from the vertical stabilizer via Equation 5.6.22 in Phillips\n",
    "    deltac_l_beta_v = -eta_v * (s_v * h_v) / (s_w * b_w) * (1 - eps_s_beta_v) * c_lv_alpha\n",
    "\n",
    "    # Get the contribution from the horizontal stabilizer via Equation 5.6.23 in Phillips (assuming conventional tail)\n",
    "    deltac_l_beta_h = + 0.08 * eta_v * (s_v * b_h) / (s_w * b_w) * (1 - eps_s_beta_v) * c_lv_alpha\n",
    "\n",
    "    # Get the total roll stability derivative\n",
    "    deltac_n_beta_v = deltac_l_beta_gammaw + deltac_l_beta_v + deltac_l_beta_h\n",
    "\n",
    "    # Print the roll stability derivative\n",
    "    print('Roll Stability Derivative: ', round(deltac_n_beta_v, 3), ' (recommended -0.1->0)', sep='')\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8e2ac8f",
   "metadata": {},
   "source": [
    "Compute everything and print to screen"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbaa65da",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the static margin for longitudinal stability\n",
    "static_margin(s_w, s_h, c_lw_alpha, c_lh_alpha, l_w, l_h, eta_h, eps_d_alpha, cbar_w)\n",
    "\n",
    "# Compute the yaw stability derivative\n",
    "yaw_derivative(eta_v, s_v, s_w, l_v, l_w, b_w, c_lv_alpha, eps_s_beta_v)\n",
    "\n",
    "# Compute the roll stability derivative\n",
    "roll_derivative(gamma, kappa_gamma, kappa_l, c_lw_alpha, h_v)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c69ee3d9",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
